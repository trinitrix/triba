mod error;

use aes_gcm_siv::{Aes256GcmSiv, KeyInit, Nonce};
pub use error::Error;
pub use interprocess::local_socket::Name as SocketName;
use std::collections::HashMap;

use interprocess::local_socket::tokio::{prelude::*, RecvHalf, Stream};
use rand::thread_rng;
use tokio::io::{AsyncReadExt, AsyncWriteExt, BufReader};
use tokio::sync::{mpsc, oneshot};
use triba_packet::{FormField, IdPool, Message, Packet, Request, Response};
use uuid::Uuid;
use x25519_dalek::{EphemeralSecret, PublicKey};

enum Event {
    ToCoreReq(Request, oneshot::Sender<Response>),
    ToCoreResp(Response, u64),
    FromCore(Packet),
}

pub struct Session {
    tx: mpsc::UnboundedSender<Event>,
}

impl Session {
    pub async fn new(
        id: Uuid,
        sock_name: SocketName<'_>,
    ) -> Result<(Self, mpsc::UnboundedReceiver<Packet>), Error> {
        let stream = Stream::connect(sock_name.clone())
            .await
            .map_err(|e| Error::SocketConnectionFailure(e.to_string()))?;

        let (sock_rx, mut sock_tx) = stream.split();
        let mut sock_rx = BufReader::new(sock_rx);

        sock_tx
            .write(id.as_bytes())
            .await
            .map_err(|e| Error::SocketWriteFailure(e.to_string()))?;

        let dh_core_public = {
            let mut buffer = [0u8; 32];
            sock_rx
                .read(&mut buffer)
                .await
                .map_err(|e| Error::SocketReadFailure(e.to_string()))?;
            PublicKey::from(buffer)
        };

        let dh_own_secret = EphemeralSecret::random_from_rng(thread_rng());
        let dh_own_public = PublicKey::from(&dh_own_secret);

        sock_tx
            .write(dh_own_public.as_bytes())
            .await
            .map_err(|e| Error::SocketWriteFailure(e.to_string()))?;

        let shared_secret = dh_own_secret.diffie_hellman(&dh_core_public);

        let nonce = {
            let mut buffer = [0u8; 12];
            sock_rx
                .read(&mut buffer)
                .await
                .map_err(|e| Error::SocketReadFailure(e.to_string()))?;
            Nonce::from(buffer)
        };

        let cipher = Aes256GcmSiv::new(shared_secret.as_bytes().into());

        let (tx, mut rx) = mpsc::unbounded_channel();

        {
            let cipher = cipher.clone();
            let nonce = nonce.clone();
            let tx = tx.clone();

            tokio::spawn(async move {
                listen_socket(&mut sock_rx, tx, cipher, nonce)
                    .await
                    .unwrap();
            });
        }

        let (core_tx, mut core_rx) = mpsc::unbounded_channel();

        {
            let mut id_pool = IdPool::new();
            let mut responses = HashMap::new();

            tokio::spawn(async move {
                loop {
                    match rx.recv().await.unwrap() {
                        Event::FromCore(p) => match p {
                            Packet::Response { req, body, .. } => {
                                let resp: oneshot::Sender<Response> =
                                    responses.remove(&req).unwrap();
                                resp.send(body).unwrap();
                            }
                            Packet::Request { .. } => {
                                core_tx.send(p).unwrap();
                            }
                        },
                        Event::ToCoreReq(body, resp_tx) => {
                            let id = id_pool.acquire();
                            responses.insert(id, resp_tx);

                            Packet::request(id, body)
                                .send(&mut sock_tx, &cipher, &nonce)
                                .await
                                .unwrap();
                        }
                        Event::ToCoreResp(body, req) => {
                            let id = id_pool.acquire();

                            Packet::response(id, req, body)
                                .send(&mut sock_tx, &cipher, &nonce)
                                .await
                                .unwrap();
                        }
                    }
                }
            });
        }

        let session = Self { tx };

        session.request_handshake_upgrade_connection().await?;

        match core_rx.recv().await.unwrap() {
            Packet::Request { id, body } => match body {
                Request::HandshakeSuccess => {
                    session.response_success(id)?;
                }
                req => {
                    return Err(Error::UnexpectedPacket(
                        Packet::request(0, Request::HandshakeSuccess),
                        Packet::request(0, req),
                    ));
                }
            },
            resp => {
                return Err(Error::UnexpectedPacket(
                    Packet::request(0, Request::HandshakeSuccess),
                    resp,
                ));
            }
        }

        Ok((session, core_rx))
    }

    async fn request_handshake_upgrade_connection(&self) -> Result<(), Error> {
        let (tx, rx) = oneshot::channel();

        self.tx
            .send(Event::ToCoreReq(Request::HandshakeUpgradeConnection, tx))
            .map_err(|e| Error::External(e.to_string()))?;

        match rx.await.map_err(|e| Error::External(e.to_string()))? {
            Response::Success => Ok(()),
            resp => Err(Error::UnexpectedPacket(
                Packet::response(0, 0, Response::Success),
                Packet::response(0, 0, resp),
            )),
        }
    }

    pub async fn request_close(&self) -> Result<(), Error> {
        let (tx, rx) = oneshot::channel();

        self.tx
            .send(Event::ToCoreReq(Request::Close, tx))
            .map_err(|e| Error::External(e.to_string()))?;

        match rx.await.map_err(|e| Error::External(e.to_string()))? {
            Response::Ackknowledged => Ok(()),
            resp => Err(Error::UnexpectedPacket(
                Packet::response(0, 0, Response::Ackknowledged),
                Packet::response(0, 0, resp),
            )),
        }
    }

    pub async fn request_account_setup_select_option(
        &self,
        options: Vec<String>,
    ) -> Result<u64, Error> {
        let (tx, rx) = oneshot::channel();

        self.tx
            .send(Event::ToCoreReq(
                Request::AccountSetupSelectOption(options),
                tx,
            ))
            .map_err(|e| Error::External(e.to_string()))?;

        match rx.await.map_err(|e| Error::External(e.to_string()))? {
            Response::AccountSetupSelectOption(option) => Ok(option),
            resp => Err(Error::UnexpectedPacket(
                Packet::response(0, 0, Response::AccountSetupSelectOption(0)),
                Packet::response(0, 0, resp),
            )),
        }
    }

    pub async fn request_account_setup_get_form(
        &self,
        form: HashMap<String, (FormField, bool)>,
    ) -> Result<HashMap<String, String>, Error> {
        let (tx, rx) = oneshot::channel();

        self.tx
            .send(Event::ToCoreReq(Request::AccountSetupGetForm(form), tx))
            .map_err(|e| Error::External(e.to_string()))?;

        match rx.await.map_err(|e| Error::External(e.to_string()))? {
            Response::AccountSetupFilledForm(form) => Ok(form),
            resp => Err(Error::UnexpectedPacket(
                Packet::response(0, 0, Response::AccountSetupFilledForm(HashMap::new())),
                Packet::response(0, 0, resp),
            )),
        }
    }

    pub async fn request_account_setup_success(&self) -> Result<(), Error> {
        let (tx, rx) = oneshot::channel();

        self.tx
            .send(Event::ToCoreReq(Request::AccountSetupSuccess, tx))
            .map_err(|e| Error::External(e.to_string()))?;

        match rx.await.map_err(|e| Error::External(e.to_string()))? {
            Response::Ackknowledged => Ok(()),
            resp => Err(Error::UnexpectedPacket(
                Packet::response(0, 0, Response::Ackknowledged),
                Packet::response(0, 0, resp),
            )),
        }
    }

    pub async fn request_account_setup_failure(&self, error: String) -> Result<(), Error> {
        let (tx, rx) = oneshot::channel();

        self.tx
            .send(Event::ToCoreReq(Request::AccountSetupFailure(error), tx))
            .map_err(|e| Error::External(e.to_string()))?;

        match rx.await.map_err(|e| Error::External(e.to_string()))? {
            Response::Ackknowledged => Ok(()),
            resp => Err(Error::UnexpectedPacket(
                Packet::response(0, 0, Response::Ackknowledged),
                Packet::response(0, 0, resp),
            )),
        }
    }

    pub async fn request_initial_sync_ready(&self) -> Result<(), Error> {
        let (tx, rx) = oneshot::channel();

        self.tx
            .send(Event::ToCoreReq(Request::InitialSyncReady, tx))
            .map_err(|e| Error::External(e.to_string()))?;

        match rx.await.map_err(|e| Error::External(e.to_string()))? {
            Response::Ackknowledged => Ok(()),
            resp => Err(Error::UnexpectedPacket(
                Packet::response(0, 0, Response::Ackknowledged),
                Packet::response(0, 0, resp),
            )),
        }
    }

    pub async fn request_chat_received_message(
        &self,
        chat: String,
        message: Message,
    ) -> Result<(), Error> {
        let (tx, rx) = oneshot::channel();

        self.tx
            .send(Event::ToCoreReq(
                Request::ChatReceivedMessage(chat, message),
                tx,
            ))
            .map_err(|e| Error::External(e.to_string()))?;

        match rx.await.map_err(|e| Error::External(e.to_string()))? {
            Response::Ackknowledged => Ok(()),
            resp => Err(Error::UnexpectedPacket(
                Packet::response(0, 0, Response::Ackknowledged),
                Packet::response(0, 0, resp),
            )),
        }
    }

    pub async fn request_chat_received_event(
        &self,
        chat: String,
        event: String,
    ) -> Result<(), Error> {
        let (tx, rx) = oneshot::channel();

        self.tx
            .send(Event::ToCoreReq(
                Request::ChatReceivedEvent(chat, event),
                tx,
            ))
            .map_err(|e| Error::External(e.to_string()))?;

        match rx.await.map_err(|e| Error::External(e.to_string()))? {
            Response::Ackknowledged => Ok(()),
            resp => Err(Error::UnexpectedPacket(
                Packet::response(0, 0, Response::Ackknowledged),
                Packet::response(0, 0, resp),
            )),
        }
    }

    pub fn response_success(&self, receiver: u64) -> Result<(), Error> {
        self.tx
            .send(Event::ToCoreResp(Response::Success, receiver))
            .map_err(|e| Error::External(e.to_string()))?;
        Ok(())
    }

    pub fn response_ackknowledged(&self, receiver: u64) -> Result<(), Error> {
        self.tx
            .send(Event::ToCoreResp(Response::Ackknowledged, receiver))
            .map_err(|e| Error::External(e.to_string()))?;
        Ok(())
    }

    pub fn response_unexpected_request(&self, receiver: u64) -> Result<(), Error> {
        self.tx
            .send(Event::ToCoreResp(Response::UnexpectedRequest, receiver))
            .map_err(|e| Error::External(e.to_string()))?;
        Ok(())
    }

    pub fn response_ressource_timeout(&self, receiver: u64) -> Result<(), Error> {
        self.tx
            .send(Event::ToCoreResp(Response::RessourceTimeout, receiver))
            .map_err(|e| Error::External(e.to_string()))?;
        Ok(())
    }
}

async fn listen_socket(
    rx: &mut BufReader<RecvHalf>,
    tx: mpsc::UnboundedSender<Event>,
    cipher: Aes256GcmSiv,
    nonce: Nonce,
) -> Result<(), Error> {
    loop {
        let packet = Packet::recv(rx, &cipher, &nonce).await?;
        tx.send(Event::FromCore(packet))
            .map_err(|e| Error::External(e.to_string()))?
    }
}
