use thiserror::Error;
use triba_packet::Packet;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Failed to connect to the socket: {0}")]
    SocketConnectionFailure(String),

    #[error("Failed to write to the socket: {0}")]
    SocketWriteFailure(String),

    #[error("Failed to read from the socket: {0}")]
    SocketReadFailure(String),

    #[error("Failed to serialize packet: {0}")]
    SerializationFailure(String),

    #[error("Failed to deserialize packet: {0}")]
    DeserializationFailure(String),

    #[error("An error occurred in the packaging process: {0}")]
    PackagingError(triba_packet::Error),

    #[error("Received packet: {0}, expected: {1}")]
    UnexpectedPacket(Packet, Packet),

    #[error("{0}")]
    External(String),
}

impl From<triba_packet::Error> for Error {
    fn from(value: triba_packet::Error) -> Self {
        Self::PackagingError(value)
    }
}
